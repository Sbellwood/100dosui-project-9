//
//  ContentView.swift
//  Drawing
//
//  Created by Skyler Bellwood on 7/24/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var thicknessAmount: CGFloat = 5.0
    @State private var colorCycle = 0.0
    
    
    var body: some View {
        VStack {
            Arrow()
                .stroke(Color.blue, style: StrokeStyle(lineWidth: thicknessAmount, lineCap: .round, lineJoin: .round))
                .frame(width: 150, height: 200)
                .padding()
                .onTapGesture {
                    withAnimation {
                        self.thicknessAmount = CGFloat.random(in: 5.0...30.0)
                    }
            }
            
            Spacer()
            
            ColorCyclingRectangle(amount: self.colorCycle)
                .frame(width: 150, height: 150)
            
            Slider(value: $colorCycle)
                .padding([.horizontal, .bottom])
        }
    }
}

struct Arrow: Shape {
    
    var lineThickness: CGFloat = 1.0
    
    var animatableData: CGFloat {
        get { lineThickness }
        set { self.lineThickness = newValue }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.maxX - rect.maxX * 0.35, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.maxX - rect.maxX * 0.35, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX + rect.maxX * 0.35, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX + rect.maxX * 0.35, y: rect.midY))
        
        return path
    }
}

struct ColorCyclingRectangle: View {
    var amount = 0.0
    var steps = 100
    
    var body: some View {
        ZStack {
            ForEach(0..<steps) { value in
                Rectangle()
                    .inset(by: CGFloat(value))
                    .strokeBorder(LinearGradient(gradient: Gradient(colors: [
                        self.color(for: value, brightness: 1),
                        self.color(for: value, brightness: 0)
                    ]), startPoint: .top, endPoint: .bottom), lineWidth: 2)
            }
        }
        .drawingGroup()
    }
    
    func color(for value: Int, brightness: Double) -> Color {
        var targetHue = Double(value) / Double(self.steps) + self.amount
        
        if targetHue > 1 {
            targetHue -= 1
        }
        
        return Color(hue: targetHue, saturation: 1, brightness: brightness)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

